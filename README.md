# lamp

The Localization and Mapping Platform (LAMP) software fuses radiation and contextual data to produce 3-D maps that locate and identify gamma-ray emitting sources in complex environments. It enables data collection, fusion, and processing onboard the LAMP system in handheld configurations, and enables near-real-time 3-D mapping from onboard an unmanned aerial system (UAS). This software enables a real-time 3-D gamma-ray mapping capability that is not available commercially.

# licensing

For licensing questions please visit https://ipo.lbl.gov. Disclosure 2020-030.
